#!/usr/bin/env python3

from setuptools import setup

# see also https://betterscientificsoftware.github.io/python-for-hpc/tutorials/python-pypi-packaging/

from fissg import __author__, __version__, __license__, __description__, __url__, __name__

setup(
    name=__name__,
    version=__version__,
    description=__description__,
    url=__url__,
    author=__author__,
    packages=[
        'fissg',
        'fissg.types',
        'fissg.data',
        'fissg.knitters',
    ],
    include_package_data=True,
    package_data={"fissg.data": ["*"]},
    install_requires=[
        'dataclass-wizard[yaml]>=0.23.0',
        'pyyaml>=6.0.2',
        'python-dateutil>=2.9.0',
        'panflute[extras]>=2.3.1',
        'pillow>=10.4.0',
        'Jinja2>=3.1.4',
    ],
    entry_points={
        'console_scripts': ['fissg=fissg.__main__:main'],
    },

    license=__license__,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication'
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Environment :: Web Environment',
        'Topic :: Internet',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
        'Topic :: Software Development :: Version Control :: Git',
        'Topic :: Text Processing :: Markup',
        'Topic :: Text Processing :: Markup :: HTML',
        'Topic :: Text Processing :: Markup :: Markdown',
    ],
)
