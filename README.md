# FISSG

[![pipeline status](https://gitlab.gwdg.de/fissg/fissg/badges/main/pipeline.svg)](https://gitlab.gwdg.de/fissg/fissg/-/commits/main)

FISSG Is a Static-Site-Generator

## Dependencies

https://pillow.readthedocs.io/en/stable/installation/building-from-source.html#external-libraries
```bash
sudo apt-get install libtiff5-dev libjpeg8-dev libopenjp2-7-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python3-tk \
    libharfbuzz-dev libfribidi-dev libxcb1-dev
```

## Flow

1. [x] Read contents directory. (-> File)
1. [x] Add git information to files.
1. [x] Execute Knitters based on configured file types and populate the Cupboard.
    - [x] .yml -> MetadataKnitter (-> Metadata)
    - [x] .md -> PandocKnitter (-> Page, Tag) (translatable)
    - [ ] .docx/.tex/.typst -> PandocKnitter (-> Page, Tag) (translatable)
    - [x] .png/.jpg/.webp -> ImageKnitter (-> Image)
    - [ ] (.pdf -> PDFKnitter? (-> Image))
1. [ ] Validate translations.
1. [x] Apply builders to build the website files based on the objects stored in the Cupboard.
    - [x] files
    - [ ] tags
    - [ ] pages
    - [ ] images
1. [ ] Write the files to the output directory.

