.PHONY: default
default: test

.PHONY: test
test: venv/bin/activate install
	[ -d "./content" ] || git clone git@gitlab.gwdg.de:GAUMI-fginfo/fg-website-data.git content
	. venv/bin/activate && \
		fissg --test

.PHONY: dist
dist: clean venv/bin/activate install
	. venv/bin/activate && \
		python3 -m build --wheel

.PHONY: install
install: venv/bin/activate
	. venv/bin/activate && \
		python3 -m pip install .

.PHONY: clean
clean:
	$(RM) -r venv/ build/ dist/ fissg.egg-info/
	find fissg/ -type d -name '__pycache__' -exec rm -rf {} +

.PHONY: testserver
testserver:
	python3 -m http.server 8000

venv/bin/activate: requirements.txt setup.py
	python3 -m venv venv
	. venv/bin/activate && \
		python3 -m pip install -r requirements.txt

