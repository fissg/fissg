import mimetypes
from pathlib import Path
from typing import Optional
from fnmatch import fnmatchcase

mimetypes.init()


type mimetype_pattern = str
"""
A mimetype pattern or list of mimetype patterns seperated by '|'.

Matching is done via fnmatchcase. Thus pattern characters like "*" and "?" can be used.
See https://docs.python.org/3/library/fnmatch.html

Leading and trailing whitespace is stripped from the individual patterns.
("  text/html | text/markdown " is equal to "text/html|text/markdown")

Examples:
- "text/markdown" matches exactly text/markdown.
- "text/*" matches text/markdown, text/html, etc...
- "application/x-tex|text/x-tex" matches application/x-tex and text/x-tex
- "*/x-tex" matches application/x-tex, text/x-tex, etc...
- "*/ogg" matches audio/ogg, video/ogg, etc...
- "audio/*midi" matches audio/midi and audio/x-midi.
- "image/*|video/*|audio/*" matches all image, audio, and video files.
- "*" matches all mimetypes.
"""


class Mimetype:
    mimetype: Optional[str]
    encoding: Optional[str]

    def __init__(self, path: Path):
        guess = mimetypes.guess_type(path.name)
        self.mimetype = guess[0]
        self.encoding = guess[1]


def does_mimetype_match(mimetype: Mimetype | str, pattern: mimetype_pattern) -> bool:
    """
    Whether a given mimetype matches a given mimetype pattern.
    """
    if isinstance(mimetype, Mimetype):
        if mimetype.mimetype is None:
            return False
        mimetype = mimetype.mimetype

    for pat in pattern.split("|"):
        if fnmatchcase(mimetype, pat.strip()):
            return True
    return False
