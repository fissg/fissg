from __future__ import annotations
from dataclasses import dataclass, field
from typing import Dict

import fissg.config as C
import fissg.types.file as F
import fissg.types.image as Img
import fissg.types.page as P
import fissg.types.link as L


@dataclass
class Cupboard:
    """
    The cupboard contains all the parsed objects required for building the site.
    """

    config: C.Config

    files: Dict[str, F.File]
    """
    The raw source files.

    The key is the name of the file.
    """

    pages: Dict[str, P.Page] = field(default_factory=dict)
    """
    The pages.

    The key is the slug of the page.

    Use get_page() if you want to get a page.
    """

    tags: Dict[str, P.Tag] = field(default_factory=dict)
    """
    The pages that are representative of a tag.

    The key is the tag name.
    """

    images: Dict[str, Img.Image] = field(default_factory=dict)
    """
    The images.

    The key is the name of the source file of the image.
    """

    links: Dict[str, L.Link] = field(default_factory=dict)
    """
    All the links.

    The key is the raw url as it was given.
    """

    def __post_init__(self):
        # Generate Links for all File objects, thus ensuring every File has at least one incoming link
        file_link_prefixes = self.config.get_prefixes_for_link_protocol(L.LinkProtocol.FILE)
        for file in self.files.values():
            for prefix in file_link_prefixes:
                self.get_link(prefix + file.name, "generated")

    def get_file(self, name: str, exception_if_not_found: bool = False) -> F.File | None:
        """
        Get a file by its name or None if it doesn't exist.

        If exception_if_not_found is True, an exception is thrown if the file doesn't exist instead of returning None.
        """
        if exception_if_not_found and name not in self.files:
            raise Exception("File not found!", name)
        return self.files.get(name, None)

    def get_image(self, name: str | F.File) -> Img.Image:
        """
        Get an image by its source file name (E.g. "ophase_2024.png") or file object.

        If the image doesn't exist, a new instance is created.
        """
        if isinstance(name, F.File):
            name = name.name
        if name not in self.images:
            file = self.get_file(name)
            if not file:
                raise Exception("Image source file not found:", name)
            image = Img.Image(file)
            self.images[name] = image

            # Generate Links for all Image objects, thus ensuring every Image has at least one incoming link
            for prefix in self.config.get_prefixes_for_link_protocol(L.LinkProtocol.IMAGE):
                self.get_link(prefix + image.file.name, "generated")
        return self.images[name]

    def get_page(self, slug: str) -> P.Page:
        """
        Get a page by its slug.

        If the slug references the slug of a tag (it has the tag slug prefix), returns the output of get_tag()

        If the page doesn't exist, a new one is created.
        """
        slug = slug.lower().strip()
        if slug.startswith(self.config.tag_slug_prefix):
            return self.get_tag(slug.removeprefix(self.config.tag_slug_prefix))
        if slug not in self.pages:
            page = P.Page(self, slug)
            self.pages[slug] = page

            # Generate Links for all Page objects, thus ensuring every Page has at least one incoming link
            for prefix in self.config.get_prefixes_for_link_protocol(L.LinkProtocol.PAGE):
                self.get_link(prefix + page.slug, "generated")
                for lang in self.config.lang.supported:
                    self.get_link(prefix + page.slug + ":" + lang, "generated")
        return self.pages[slug]

    def get_tag(self, tag: str) -> P.Tag:
        """
        Get a tag by its tag name.

        If the tag doesn't exist, a new one is created.
        """
        tag = tag.lower().strip()
        if tag not in self.tags:
            page = P.Tag(self, "tag-" + tag)
            self.tags[tag] = page

            # Generate Links for all Tag objects, thus ensuring every Tag has at least one incoming link
            for prefix in self.config.get_prefixes_for_link_protocol(L.LinkProtocol.PAGE):
                self.get_link(prefix + page.slug, "generated")
                for lang in self.config.lang.supported:
                    self.get_link(prefix + page.slug + ":" + lang, "generated")
            for prefix in self.config.get_prefixes_for_link_protocol(L.LinkProtocol.TAG):
                self.get_link(prefix + page.tag, "generated")
                for lang in self.config.lang.supported:
                    self.get_link(prefix + page.tag + ":" + lang, "generated")
        return self.tags[tag]

    def get_link(self, raw: str, src: L.Linksource | None) -> L.Link:
        if raw not in self.links:
            link = L.Link(self, raw)
            if src:
                link.add_source(src)
            self.links[raw] = link
        return self.links[raw]
