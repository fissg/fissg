from __future__ import annotations
from pathlib import PurePath
from typing import Dict
import fissg.cupboard as Cup
import fissg.theme as T
import fissg.types.link as L


def build_raw_files(cupboard: Cup.Cupboard, _: T.Theme, output: Dict[PurePath, bytes]):
    prefix = cupboard.config.get_prefixes_for_link_protocol(L.LinkProtocol.FILE)[0]
    for file in cupboard.files.values():
        # output[file.path_in_content_dir] = file.content_bytes
        link = cupboard.get_link(prefix + file.name, None)
        output[link.resolve()] = file.content_bytes


def build_pages(cupboard: Cup.Cupboard, theme: T.Theme, output: Dict[PurePath, bytes]):
    prefix = cupboard.config.get_prefixes_for_link_protocol(L.LinkProtocol.PAGE)[0]
    for page in cupboard.pages.values():
        if page.is_tag:
            continue
        for lang in cupboard.config.lang.supported:
            if lang not in page.translations:
                raise Exception("Page is missing translation.", lang, page)
            link = cupboard.get_link(prefix + page.slug, None)
            path = link.resolve(lang=lang)
            res = theme.render(page, lang, path)
            output[path] = res.encode(encoding='utf-8')


def build_tags(cupboard: Cup.Cupboard, theme: T.Theme, output: Dict[PurePath, bytes]):
    prefix = cupboard.config.get_prefixes_for_link_protocol(L.LinkProtocol.TAG)[0]
    for tag in cupboard.tags.values():
        assert tag.is_tag, "Tag is not a tag???"
        for lang in cupboard.config.lang.supported:
            if len(tag.translations) > 0 and lang not in tag.translations:
                raise Exception("Tag is missing translation.", lang, tag)
            link = cupboard.get_link(prefix + tag.tag, None)
            path = link.resolve(lang=lang)
            res = theme.render(tag, lang, path)
            output[path] = res.encode(encoding='utf-8')


builders = [
    build_raw_files,
    build_pages,
    build_tags,
]
