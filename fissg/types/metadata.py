from __future__ import annotations
import copy
from dataclasses import dataclass, field
from typing import Dict, List, Literal, Optional, Tuple, cast
from dataclass_wizard import JSONWizard
from datetime import datetime
import dateutil.parser

import fissg.config as C

status_types = Literal["draft", "hidden", "skip", "published"]


@dataclass(kw_only=True)
class Metadata(JSONWizard):
    """
    Metadata for a file, page, image, etc...
    """
    class _(JSONWizard.Meta):
        key_transform_with_load = "NONE"
        key_transform_with_dump = "NONE"

    raw: List[Tuple[Dict, str]] = field(default_factory=list)
    """
    The raw dictionaries from which the Metadata instance was parsed.

    The string in the tuple is an indicator from the source of the Metadata, thus one can later reason about where a specific value comes from, at least in theory.

    When merging two Metadata instances, the contents of raw gets concatenated.
    """

    # FISSG
    slug: Optional[str] = None
    status: Optional[status_types] = None
    modified: Optional[str | datetime] = None
    tags: List[str] = field(default_factory=list)
    authors: List[str] = field(default_factory=list)
    template: Optional[str] = None

    # https://pandoc.org/MANUAL.html#metadata-blocks
    # https://pandoc.org/MANUAL.html#metadata-variables
    title: Optional[str] = None
    date: Optional[str | datetime] = None
    subtitle: Optional[str] = None
    abstract: Optional[str] = None
    subject: Optional[str] = None
    description: Optional[str] = None
    category: Optional[str] = None

    # https://pandoc.org/MANUAL.html#language-variables
    lang: Optional[str] = None
    dir: Optional[Literal["ltr", "rtl"]] = None

    # https://pandoc.org/MANUAL.html#variables-for-html
    # TODO

    def normalize(self) -> None:
        """
        Convert every attribute to its most explicit representation whereever possible and copy values to their aliases.

        Convert date to datetime.
        Convert modified to datetime.

        Sort all lists and remove duplicates.
        """
        # Parse dates.
        if isinstance(self.date, str):
            self.date = dateutil.parser.parse(self.date)
        if isinstance(self.modified, str):
            self.modified = dateutil.parser.parse(self.modified)

        # Sort all lists and remove duplicates.
        self.authors = sorted(set(self.authors))
        self.tags = sorted(set(self.tags))

    def merge(self, other: Metadata) -> Metadata:
        """
        Merge other into self.

        This function will call normalize() on both self and other.

        Returns:
        self
        """
        self.normalize()
        other.normalize()

        if other.slug:
            self.slug = other.slug  # Optional[str]
        if other.status:
            self.status = other.status  # Optional[Literal["draft"] | Literal["hidden"] | Literal["skip"] | Literal["published"]]
        if other.modified:
            self.modified = other.modified  # Optional[str | datetime]
        if self.tags and other.tags and self.tags != other.tags:
            raise Exception("Cannot merge tags.", self.tags, other.tags)
        elif other.tags:
            self.tags = cast(List[str], other.tags).copy()  # List[str]
        if self.authors and other.authors and self.authors != other.authors:
            raise Exception("Cannot merge authors.", self.authors, other.authors)
        elif other.authors:
            self.authors = cast(List[str], other.authors).copy()  # str | List[str]
        if other.template:
            self.template = other.template  # Optional[str]
        if other.title:
            self.title = other.title  # Optional[str]
        if other.date:
            self.date = other.date  # Optional[str | datetime]
        if other.subtitle:
            self.subtitle = other.subtitle  # Optional[str]
        if other.abstract:
            self.abstract = other.abstract  # Optional[str]
        if other.subject:
            self.subject = other.subject  # Optional[str]
        if other.description:
            self.description = other.description  # Optional[str]
        if other.category:
            self.category = other.category  # Optional[str]
        if other.lang:
            self.lang = other.lang  # Optional[str]
        if other.dir:
            self.dir = other.dir  # Optional[Literal["ltr"] | Literal["rtl"]]

        self.raw = self.raw + other.raw
        self.normalize()
        return self

    def copy(self) -> Metadata:
        """
        Create a deep copy of this instance.
        """
        self.normalize()
        res = copy.deepcopy(self)
        res.normalize()
        return res

    @staticmethod
    def default(config: C.Config) -> Metadata:
        """
        Get a Metadata instance with default values.

        Note: Just calling Metadata() does not have all default values set, as merging might not work then.
        """
        res = Metadata(
            # slug: Optional[str] = None
            status=config.default_page_status,
            modified=datetime.now(),
            tags=[],
            authors=[],
            # template: Optional[str] = None
            # title: Optional[str] = None
            date=datetime.now(),
            # subtitle: Optional[str] = None
            # abstract: Optional[str] = None
            # subject: Optional[str] = None
            # description: Optional[str] = None
            # category: Optional[str] = None
            # lang: Optional[str] = None
            dir="ltr",
        )
        res.normalize()
        return res
