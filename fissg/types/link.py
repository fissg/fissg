from __future__ import annotations
from enum import Enum
from pathlib import PurePath
from typing import Literal, Optional, Set, Tuple
from urllib.parse import ParseResult, urlparse
import fissg.config as C
import fissg.cupboard as Cup
import fissg.types.page as P
import fissg.types.file as F
import fissg.types.image as Img

type Linksource = P.Document | P.Page | P.Tag | Literal["generated"]


class LinkProtocol(Enum):
    URL = "URL"
    """
    An external URL. That should be used as is.

    https://developer.mozilla.org/en-US/docs/Web/URI/Schemes
    https://docs.python.org/3/library/urllib.parse.html
    """
    IMAGE = "IMAGE"
    TAG = "TAG"
    PAGE = "PAGE"
    FILE = "FILE"

    @staticmethod
    def determine_linkprotocol(config: C.Config, raw: str) -> Tuple[LinkProtocol, str]:
        schemes = config.link_protocols.keys()  # Known schemes
        schemes = filter(lambda s: raw.startswith(s), schemes)  # Determine which schemes fit
        schemes = sorted(schemes, key=lambda s: len(s), reverse=True)  # Prefer longest match
        if not schemes:
            raise Exception("Cannot determine LinkProtocol of url with unknown scheme.", raw, config.link_protocols.keys())
        return (LinkProtocol(config.link_protocols[schemes[0]]), schemes[0])


class Link:
    """
    Represents a link, either internal or external.
    """

    cupboard: Cup.Cupboard
    raw: str
    raw_without_scheme: str
    raw_without_scheme_nor_fragment: str
    protocol: LinkProtocol
    scheme: str
    fragment: Optional[str] = None
    """
    The anchor.
    """
    sources: Set[Linksource]
    """
    Sources that contain this link.
    """

    is_external: bool
    target_url: Optional[ParseResult] = None
    target_file: Optional[F.File] = None
    target_image: Optional[Img.Image] = None
    target_page: Optional[P.Page] = None
    target_tag: Optional[P.Tag] = None
    target_lang: Optional[C.langcode] = None

    def __init__(self, cupboard: Cup.Cupboard, raw: str) -> None:
        """
        Don't use this constructor. Use Cupboard.get_link() instead.
        """
        if raw.startswith('#'):
            raise Exception("Link objects are not designed to handle local links (those starting with '#').", raw)
        self.cupboard = cupboard
        self.raw = raw
        self.sources = set()
        self._parse_raw()

    def _parse_raw(self) -> None:
        # TODO handle links to local files or pages without scheme or file extension
        # e.g.:
        # - "![](ophase_2024.png)"
        # - "![](ophase_2024)"
        # - "![](img:ophase_2024)"
        # - "![](file:ophase_2024)"
        # - "![](../images/ophase_2024.png)"
        # - "[hier](ophase.de.md)"
        # - "[hier](ophase.de)"
        # - "[hier](../event/ophase.de.md)"
        self.protocol, self.scheme = LinkProtocol.determine_linkprotocol(self.cupboard.config, self.raw)
        self.raw_without_scheme = self.raw.removeprefix(self.scheme)
        if "#" in self.raw:
            parts = self.raw.split("#")
            if len(parts) != 2:
                raise Exception("Invalid URL. URLs can only contain at most one '#'!", self.raw)
            self.fragment = parts[1]
        else:
            self.fragment = None
        self.raw_without_scheme_nor_fragment = self.raw_without_scheme.removesuffix("#" + (self.fragment or ""))
        self.is_external = self.protocol == LinkProtocol.URL
        self.target_url = None
        match self.protocol:
            case LinkProtocol.URL:
                self.target_url = urlparse(self.raw)
            case LinkProtocol.IMAGE:
                self.target_image = self.cupboard.get_image(self.raw_without_scheme_nor_fragment)
                self.target_file = self.target_image.file
                self.target_image.incoming_links.add(self)
                self.target_file.incoming_links.add(self)
            case LinkProtocol.PAGE | LinkProtocol.TAG:
                slug_or_tag = self.raw_without_scheme_nor_fragment
                parts = slug_or_tag.split(":")
                if len(parts) > 2:
                    raise Exception("Invalid slug or tag Link. Too many ':'.", self.raw)
                self.target_lang = parts[1] if len(parts) == 2 else None
                slug_or_tag = parts[0] if len(parts) == 2 else slug_or_tag
                if self.protocol == LinkProtocol.TAG:
                    self.target_tag = self.cupboard.get_tag(slug_or_tag)
                    self.target_page = self.target_tag
                else:
                    self.target_page = self.cupboard.get_page(slug_or_tag)
                self.target_page.incoming_links.add(self)
                # At the time a Link to a page is created, it might not have all translations initialized.
                # Thus we cannot initiate self.target_file nor could we create something like self.target_document.
                # They would be prone to race conditions.
            case LinkProtocol.FILE:
                self.target_file = self.cupboard.get_file(self.raw_without_scheme_nor_fragment)
                if not self.target_file:
                    raise Exception("Linked to file doesn't exist.", self.raw)
                self.target_file.incoming_links.add(self)

    @property
    def is_internal(self) -> bool:
        return not self.is_external

    def add_source(self, src: Linksource):
        self.sources.add(src)

    def resolve(self, *,
                relative_to_dir: PurePath | str = PurePath('.'),
                lang: Optional[C.langcode] = None,
                # resolution,
                # force: Optional[Literal["image", "source_file"]] = None,
                ) -> PurePath:
        """
        Resolve a link to an actual path that can be used on the website.
        """

        relative_to_dir = PurePath(relative_to_dir) if isinstance(relative_to_dir, str) else relative_to_dir
        relative_to_dir_num_back = len(list(filter(lambda p: p == "..", relative_to_dir.parent.parts)))
        relative_to_dir_num = len(list(filter(lambda p: p != ".." and p != ".", relative_to_dir.parent.parts)))
        relative_num = relative_to_dir_num - relative_to_dir_num_back

        base = PurePath("../" * relative_num)
        match self.protocol:
            case LinkProtocol.URL:
                raise Exception("An external URL cannot be resolved to a path!", self)
            case LinkProtocol.IMAGE | LinkProtocol.FILE:
                # TODO implement resolve for images that respects self.target_image and resolutions
                assert self.target_file
                return base / "file" / self.target_file.name
            case LinkProtocol.PAGE:
                lang = self.target_lang or lang
                if not lang:
                    raise Exception("Cannot resolve link to a page without a language!", self)
                assert self.target_page
                return base / lang / self.target_page.category.tag / (self.target_page.slug + ".html")
            case LinkProtocol.TAG:
                lang = self.target_lang or lang
                if not lang:
                    raise Exception("Cannot resolve link to a tag without a language!", self)
                assert self.target_tag
                return base / lang / "tag" / (self.target_tag.tag + ".html")
        # TODO prevent case where relative_to_dir and res have common ancestor and thus unnecessary "../" are used in relative path

    def __eq__(self, value: object) -> bool:
        if not isinstance(value, Link):
            return False
        return self.raw == value.raw

    def __hash__(self) -> int:
        return hash(self.raw)

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        match self.protocol:
            case LinkProtocol.URL:
                return f"LinkURL('{self.raw}', {self.target_url})"
            case LinkProtocol.IMAGE:
                return f"LinkIMAGE('{self.raw}', {self.target_image})"
            case LinkProtocol.PAGE:
                return f"LinkPAGE('{self.raw}', {self.target_page}, lang: {self.target_lang})"
            case LinkProtocol.TAG:
                return f"LinkTAG('{self.raw}', {self.target_tag}, lang: {self.target_lang})"
            case LinkProtocol.FILE:
                return f"LinkFILE('{self.raw}', {self.target_file})"
