from __future__ import annotations
from pathlib import Path
from typing import List, Literal, Optional, Set
from dataclasses import dataclass
import hashlib

import fissg.config as C
import fissg.git as git
from fissg.mimetype import Mimetype
import fissg.types.link as L
from fissg.types.metadata import Metadata


@dataclass
class Author:
    name: str
    domain: str
    local_part: str


class File:
    """
    Represents a file in the contents directory.
    """

    name: str
    path_in_content_dir: Path
    parent_dir_type: Literal["theme", "content"]
    absolute: Path
    content_bytes: bytes
    content_text: Optional[str]
    content_lines: Optional[List[str]]
    sha256sum: str
    mimetype: Mimetype
    authors: List[Author]
    last_modification_author: Optional[Author]

    metadata: Metadata
    metadata_file: Optional[File]

    incoming_links: Set[L.Link]
    """
    Links that refer to this file.
    """

    def __init__(self, path_in_content_dir: Path, absolute: Path, config: C.Config, parent_dir_type: Literal["theme", "content"] = "content") -> None:
        self.path_in_content_dir = path_in_content_dir
        self.parent_dir_type = parent_dir_type
        self.absolute = absolute

        self.name = path_in_content_dir.name

        self.metadata = Metadata.default(config)
        self.metadata_file = None

        self.incoming_links = set()

        # Read contents
        with absolute.open("rb") as f:
            self.content_bytes = f.read()
        try:
            self.content_text = self.content_bytes.decode(encoding="utf-8", errors="strict")
            self.content_lines = self.content_text.splitlines()
        except UnicodeError:
            # File is not a text file
            self.content_text = None
            self.content_lines = None

        # Basic meta information
        self.sha256sum = hashlib.sha256(self.content_bytes).hexdigest()
        self.mimetype = Mimetype(path_in_content_dir)

        self.metadata.date, self.metadata.modified = git.git_file_info_date(absolute)
        self.authors, self.last_modification_author = git.git_file_info_author(absolute)

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"File({self.name}, path: {self.path_in_content_dir}, mimetype: {self.mimetype.mimetype}, sha256sum: {self.sha256sum})"
