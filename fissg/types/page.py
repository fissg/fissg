from __future__ import annotations
from dataclasses import dataclass
from os import path
from pathlib import Path
from typing import Dict, Optional, Set, Tuple
import fissg.config as C
import fissg.types.file as F
import fissg.types.link as L
from fissg.types.metadata import Metadata
import fissg.cupboard as Cup

import panflute as pf


def slug_from_filename(name: str) -> Tuple[str, Optional[C.langcode]]:
    """
    Try to parse a possible slug and lang from a filename.
    """
    name, _ = path.splitext(name)  # remove file extension
    parts = name.split('.')
    if not parts:
        raise Exception("filename is empty?", name)

    slug = parts[0].lower().strip()
    lang = parts[-1].lower().strip() if len(parts) >= 2 else None

    return (slug, lang)


@dataclass
class Document:
    lang: C.langcode
    page: Page
    """
    The page this document is related to.
    """
    file: F.File
    """
    The source file.
    """
    metadata: Metadata
    content: pf.ListContainer

    def __hash__(self) -> int:
        return hash(pf.stringify(self.content))


class Page:
    """
    Represents a page or tag.

    Pages can be in multiple languages.
    """

    slug: str
    translations: Dict[C.langcode, Document]
    cupboard: Cup.Cupboard

    category: Tag
    tags: Set[Tag]
    is_tag: bool

    incoming_links: Set[L.Link]
    """
    Links that refer to this page.
    """

    def __init__(self, cupboard: Cup.Cupboard, slug: str) -> None:
        """
        Don't use this constructor. Use Cupboard.get_page() instead.
        """
        slug = slug.lower().strip()
        self.slug = slug
        self.translations = {}
        self.cupboard = cupboard
        self.tags = set()
        self.incoming_links = set()
        if self.slug != cupboard.config.tag_slug_prefix + cupboard.config.default_category:
            self.category = self.cupboard.get_tag(cupboard.config.default_category)
        else:
            if not isinstance(self, Tag):
                raise Exception("Slug is " + self.slug + ", but type isn't Tag???")
            self.category = self
        self.is_tag = False

    def add_translation(self, lang: C.langcode, file: F.File, metadata: Metadata, document: pf.Doc) -> Document:
        lang = lang.lower().strip()
        if lang in self.translations:
            raise Exception("Page has multiple translations for the same language.", self.slug, lang, file.name, self.translations[lang].file.name)
        metadata.normalize()
        doc = Document(lang, self, file, metadata, document.content)
        self.translations[lang] = doc
        category = metadata.category
        if not category:
            if file.path_in_content_dir.parent == Path("."):
                # files that are not in a subdirectoy get the default category
                category = self.cupboard.config.default_category
            else:
                # infer category from subdirectory
                if len(file.path_in_content_dir.parents) != 2:
                    raise Exception("Content files are not allowed to be deeper than one directory level if they don't specify their own category, as the directory is used for the category in those instances.", file.path_in_content_dir)
                category = str(file.path_in_content_dir.parents[0])
        if self.category.tag != self.cupboard.config.default_category and category != self.category.tag:
            raise Exception("Page has translations in different categories!", self.slug, self.category.tag, category)
        self.category = self.cupboard.get_tag(category)
        self.category.is_category = True
        self.category.pages.add(self)
        for tag_name in metadata.tags:
            tag = self.cupboard.get_tag(tag_name)
            self.tags.add(tag)
            tag.pages.add(self)
        return doc

    def __eq__(self, value: object, /) -> bool:
        if not isinstance(value, Page):
            return False
        return self.slug == value.slug

    def __lt__(self, value: object, /) -> bool:
        if not isinstance(value, Page):
            return False
        return self.slug < value.slug

    def __hash__(self) -> int:
        return hash(self.slug)

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Page(slug: {self.slug}, translations: {sorted(self.translations.keys())}, category: {self.category.tag})"


class Tag(Page):
    tag: str
    pages: Set[Page]

    is_category: bool

    def __init__(self, cupboard: Cup.Cupboard, slug: str) -> None:
        """
        Don't use this constructor. Use Cupboard.get_tag() or Cupboard.get_page() instead.
        """
        super().__init__(cupboard, slug)
        if not self.slug.startswith(cupboard.config.tag_slug_prefix):
            raise Exception("Tag slug doesn't start with '" + cupboard.config.tag_slug_prefix + "':", self.slug)
        self.is_tag = True
        self.is_category = False
        self.tag = self.slug.removeprefix(cupboard.config.tag_slug_prefix)
        self.pages = set()

    def __repr__(self) -> str:
        return f"Tag({self.tag}, slug: {self.slug}, translations: {self.translations.keys()}, category: {self.category.tag}, is_category: {self.is_category}, pages: {len(self.pages)})"
