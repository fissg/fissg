from __future__ import annotations
from typing import Set
import fissg.types.file as F
from PIL import Image as Img
import io

import fissg.types.link as L


class Image:
    """
    Represents an image.

    See https://pillow.readthedocs.io/en/stable/reference/Image.html
    """

    file: F.File
    img: Img.Image

    incoming_links: Set[L.Link]
    """
    Links that refer to this image.
    """

    def __init__(self, file: F.File) -> None:
        """
        Don't use this constructor. Use Cupboard.get_image() instead.
        """
        self.file = file
        self.incoming_links = set()

        self.img = Img.open(io.BytesIO(self.file.content_bytes))
        self.img.load()

        # TODO close self.img? https://pillow.readthedocs.io/en/stable/reference/open_files.html#file-handling

    def __str__(self) -> str:
        return self.__repr__()

    def __repr__(self) -> str:
        return f"Image({self.img}, {self.file})"
