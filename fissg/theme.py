import logging
from pathlib import Path, PurePath
import sys
import time
from typing import Dict
from jinja2 import Environment, FileSystemLoader
import concurrent.futures

import fissg.config as C
import fissg.types.file as F
import fissg.types.page as P

logger = logging.getLogger(__name__)


class Theme:
    env: Environment
    theme_dir: Path
    config: C.Config

    def __init__(self, theme_dir: Path, config: C.Config) -> None:
        self.theme_dir = theme_dir
        self.config = config
        templates_dir = theme_dir / "templates"
        if not templates_dir.is_dir():
            raise Exception("'templates' dir doesn't exist in theme dir.", templates_dir)
        loader = FileSystemLoader(templates_dir, followlinks=False)
        self.env = Environment(
            loader=loader,
            autoescape=True,
        )

    def read_content_files(self) -> Dict[str, F.File]:
        """
        Reads all content files included in the theme.
        """
        logger.info("Reading content files included in theme dir: %s", self.theme_dir)
        res: Dict[str, F.File] = {}
        t1 = time.perf_counter(), time.process_time()

        content_dir = self.theme_dir / "content"

        if not content_dir.is_dir():
            raise Exception("'content' dir doesn't exist in theme dir.", content_dir)

        def create_file(relative: Path, absolute: Path) -> F.File:
            logger.debug("Reading file: %s", relative)
            return F.File(relative, absolute, self.config, parent_dir_type="theme")

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = []
            for path in content_dir.rglob('*'):
                if not path.is_file():
                    continue
                absolute = path.resolve()
                relative = path.relative_to(content_dir)
                if any(map(lambda f: f.startswith("."), relative.parts)):
                    continue  # ignore hidden files and files in hidden directories
                futures.append(executor.submit(create_file, relative, absolute))

            for future in concurrent.futures.as_completed(futures):
                file = future.result()
                if file.name in res:
                    logger.critical("Duplicate file found! '%s' and '%s' have the same name which is not allowed.", res[file.name].path_in_content_dir, file.path_in_content_dir)
                    sys.exit(1)
                res[file.name] = file

        t2 = time.perf_counter(), time.process_time()
        logger.info("Reading %d files from theme dir took %.2f seconds (cpu: %.2f seconds)", len(res), t2[0] - t1[0], t2[1] - t1[1])
        return res

    def render(self, page: P.Page | P.Tag, lang: C.langcode, destination_file: PurePath) -> str:
        document = page.translations.get(lang, None)

        template_name = "page.html" if not page.is_tag else "tag.html"
        if document and document.metadata.template:
            template_name = document.metadata.template
        template = self.env.get_template(template_name)

        context = {
            "template": template_name,
            "lang": lang,
            "document": document,
            "page": page,
            "cupboard": page.cupboard,
            "config": self.config,
            "destination_file": destination_file,
            "destination_dir": destination_file.parent,
        }
        return template.render(context)
#
