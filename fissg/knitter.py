from __future__ import annotations
from abc import ABC, abstractmethod

import fissg.config as C
from fissg.mimetype import does_mimetype_match
from fissg.types.file import File
import fissg.cupboard as Cup


class Knitter(ABC):
    """
    Abstract base class for knitters.
    """

    name: str
    """
    The name of this Knitter instance.

    It's mostly just used for logging purposes.
    """

    mimetypes: C.mimetype_pattern
    """
    Which mimetypes this Knitter instance should handle.
    """

    knitter_config: C.knitter_configs
    """
    The configuration for this Knitter instance.
    """

    cupboard: Cup.Cupboard

    def __init__(self, name: str, mimetypes: C.mimetype_pattern, knitter_config: C.knitter_configs, cupboard: Cup.Cupboard) -> None:
        super().__init__()
        self.name = name
        self.mimetypes = mimetypes
        self.knitter_config = knitter_config
        self.cupboard = cupboard

    @abstractmethod
    def knit(self, file: File) -> None:
        """
        Parse a given file, generate some objects from that file (and maybe some additional information from the cupboard) and add those objects to the cupboard.
        """
        pass

    def can_knit(self, file: File) -> bool:
        """
        Whether this Knitter instance can knit this file.

        In most implementations this will just look at the mimetype (which is also the default).
        """
        return does_mimetype_match(file.mimetype, self.mimetypes)
