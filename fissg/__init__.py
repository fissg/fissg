#!/usr/bin/env python3

"""
FISSG.

FISSG Is a Static-Site-Generator
"""

__version__ = '0.1.0'

__author__ = 'Jake'
__license__ = 'CC0-1.0'
__name__ = 'fissg'
__description__ = 'FISSG Is a Static-Site-Generator'
__url__ = 'https://gitlab.gwdg.de/fissg/fissg'
