from __future__ import annotations
import subprocess
from pathlib import Path
from typing import List, Optional, Tuple
from datetime import datetime

import fissg.types.file as F


def _extract_author(raw) -> F.Author:
    author_split = raw.split('@')
    author_local_part = author_split[0]
    author_domain = author_split[1]
    author_name = '@'.join(author_split[2:])
    return F.Author(author_name, author_domain, author_local_part)


def git_file_info_date(absolute_path: Path) -> Tuple[datetime, datetime]:
    """
    Get creation and last modification date of a file.

    If the file is not tracked via git, or git is not installed, the stat last modification time is used for both values.

    Returns: (date_created, date_modified)
    """
    stat = absolute_path.stat()

    date_modified = datetime.fromtimestamp(stat.st_mtime)
    date_created = datetime.fromtimestamp(stat.st_mtime)

    date_changes = run_git(absolute_path, "log", ["--follow", "--format=%ad", "--date", "iso-strict"]).splitlines()
    if (len(date_changes) > 0):
        date_modified = datetime.fromisoformat(date_changes[0])
        date_created = datetime.fromisoformat(date_changes[-1])

    return (date_created, date_modified)


def git_file_info_author(absolute_path: Path) -> Tuple[List[F.Author], Optional[F.Author]]:
    """
    Get the authors that worked on the file and the last author that modified that file.

    If the file is not tracked via git, or git is not installed, empty values are used.

    Returns: (authors, last_modification_author)
    """
    # author
    authors_raw = run_git(absolute_path, "log", ["--follow", "--format=%aE@%aN", "--use-mailmap"]).splitlines()
    authors: List[F.Author] = []
    known_author_raws = []
    for author_raw in authors_raw:
        if author_raw not in known_author_raws:
            authors.append(_extract_author(author_raw))
            known_author_raws.append(author_raw)
    if len(authors_raw) > 0:
        last_modification_author = _extract_author(authors_raw[0])
    else:
        last_modification_author = None
    return (authors, last_modification_author)


def run_git(absolute_path: Path, subcmd: str, extra_args: List[str]) -> str:
    filename = absolute_path.name
    dir_path = absolute_path.parent
    git_bin = "git"
    args = [git_bin, subcmd] + extra_args + ["--", filename]
    p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=dir_path)
    out, _ = p.communicate("".encode('utf-8', errors='strict'))
    out_str = out.decode('utf-8')
    return out_str
