from fissg.knitter import Knitter

from fissg.types.file import File


class ImageKnitter(Knitter):
    def knit(self, file: File) -> None:
        self.cupboard.get_image(file)
