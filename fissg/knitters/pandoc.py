import subprocess
from typing import Dict, List, Optional, Tuple, cast
import panflute as pf
from panflute.elements import from_json
from panflute.utils import json

from fissg.config import PandocKnitterConfig
from fissg.cupboard import Cupboard
from fissg.knitter import Knitter

from fissg.types.file import File
from fissg.types.image import Image
from fissg.types.link import Link
from fissg.types.metadata import Metadata
from fissg.types.page import Document, slug_from_filename

# https://scorreia.com/software/panflute/


def run_pandoc(pandoc_bin: str, file: File | bytes, base: str, extensions: Dict[str, bool | None], extra_args: List[str]) -> Tuple[Dict, pf.Doc]:
    """
    Run pandoc on a given file and return the parsed document and its metadata.

    The document is parsed via

    Parameters:
    pandoc_bin: The name of or path to the pandoc binary. (E.g. "pandoc")
    file: The file or the contents of the file to be parsed.
    base: The base format. See https://pandoc.org/MANUAL.html#option--from
    extensions: The extensions that should be explicitly enabled (true) or explicitly disabled (false). A value of None means the extension will neither be explicitly enabled nor disabled. (It won't be added to the --from argument at all.) See https://pandoc.org/MANUAL.html#extensions
    extra_args: Extra arguments that should be given to the pandoc command.

    Returns:
    A tuple containing the (1.) metadata as a generic dictionary and (2.) the document.
    """
    content = file.content_bytes if isinstance(file, File) else file

    ext_str = "".join(map(lambda x: (("+" if x[1] else "-") + x[0]), filter(lambda x: x[1] is not None, extensions.items())))
    args = [pandoc_bin, "-f", base + ext_str, "-t", "json"] + extra_args
    p = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    out, _ = p.communicate(content)

    # Parse document
    doc = json.loads(out.decode('utf-8'), object_hook=from_json)
    assert isinstance(doc, pf.Doc)
    doc.format = "json"

    metadata = cast(Dict, doc.get_metadata())  # type: ignore

    return metadata, doc


class LinkWrapper(pf.Link):
    link: Optional[Link]
    """
    The parsed url attribute.

    A value of None means the link is local. (E.g. '#top')
    """


class ImageWrapper(pf.Image):
    link: Link
    """
    The parsed url attribute.
    """
    image: Optional[Image]
    """
    The referenced image.

    Same value as link.target_image.

    A value of None means the image is hosted on an external site.

    TODO: Ban images from specific external sources like pad.gwdg.de as those will be deleted at some point.
    """


def action_parse_links(element: pf.Element, document: pf.Doc, *, cupboard: Cupboard, pagedoc: Document, **_):
    """
    Post processor to update pf.Image and pf.Link elements to contain a link attribute for the parsed url.
    """
    assert isinstance(document, pf.Doc)  # Unnecessary assert, just here to get pyright to shut up about document not being used...
    if isinstance(element, pf.Link):
        assert isinstance(element.identifier, str)
        assert isinstance(element.classes, list)
        assert isinstance(element.attributes, dict)
        res = LinkWrapper(*element.content, url=element.url, title=element.title, identifier=element.identifier, classes=element.classes, attributes=element.attributes)
        if not element.url.startswith('#'):
            res.link = cupboard.get_link(element.url, pagedoc)
        else:
            res.link = None  # local link
        return res
    elif isinstance(element, pf.Image):
        link = cupboard.get_link(element.url, pagedoc)
        res = ImageWrapper(*element.content, url=element.url, title=element.title, identifier=element.identifier, classes=element.classes, attributes=element.attributes)
        res.link = link
        res.image = link.target_image
        return res


class PandocKnitter(Knitter):
    def knit(self, file: File) -> None:
        knitter_config = cast(PandocKnitterConfig, self.knitter_config)

        # Run pandoc
        pandoc_bin = knitter_config.pandoc_bin or self.cupboard.config.pandoc_bin or "pandoc"
        metadata_raw, document = run_pandoc(pandoc_bin, file, knitter_config.base, knitter_config.extensions, knitter_config.extra_args)

        # Parse and merge metadata
        metadata = Metadata.from_dict(metadata_raw)
        metadata.raw.append((metadata_raw, "pandoc"))
        authors: List[str] | str = metadata_raw.get("authors", metadata_raw.get("author", []))
        if isinstance(authors, str):
            authors = list(map(lambda s: s.strip(), authors.split(",")))
        metadata.authors = authors
        tags: List[str] | str = metadata_raw.get("tags", metadata_raw.get("keywords", []))
        if isinstance(tags, str):
            tags = list(map(lambda s: s.strip(), tags.split(",")))
        metadata.tags = tags
        metadata = file.metadata.copy().merge(metadata)

        # Determine slug and lang
        slug, lang = slug_from_filename(file.name)
        slug = metadata.slug or slug
        lang = metadata.lang or lang or self.cupboard.config.lang.default

        # Add page to cupboard
        page = self.cupboard.get_page(slug)
        pagedoc = page.add_translation(lang, file, metadata, document)

        # Apply post-processing actions
        actions = [
            action_parse_links,
        ]
        pf.run_filters(
            actions,
            doc=document,
            stop_if=None,
            # custom named arguments for actions
            cupboard=self.cupboard,
            knitter_config=knitter_config,
            slug=slug,
            lang=lang,
            metadata=metadata,
            file=file,
            pagedoc=pagedoc,
        )
