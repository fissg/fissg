import logging
from os import path
from typing import Dict
import yaml

from fissg.knitter import Knitter

from fissg.types.file import File
from fissg.types.metadata import Metadata

logger = logging.getLogger(__name__)


class MetadataKnitter(Knitter):
    def knit(self, file: File) -> None:
        ref_name, _ = path.splitext(file.name)
        ref = self.cupboard.get_file(ref_name)
        if not ref:
            logger.warning("Metadata file '%s' found, but the referenced file '%s' does not exist.", file.name, ref_name)
            return

        assert file.content_text, "Metadata file must be a text file, not binary."
        metadata_raw = yaml.safe_load(file.content_text)
        assert isinstance(metadata_raw, Dict), "Metadata must be a valid dictionary."
        metadata = Metadata.from_dict(metadata_raw)
        metadata.raw.append((metadata_raw, "metadata_file"))

        if ref.metadata_file is not None:
            raise Exception("File has multiple metadata files!", ref.name, ref.metadata_file.name, file.name)

        ref.metadata.merge(metadata)
        ref.metadata_file = file
