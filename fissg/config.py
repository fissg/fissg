from __future__ import annotations
from abc import ABC
from dataclasses import dataclass, field
import importlib
from pathlib import Path
from dataclass_wizard import JSONWizard, LoadMeta
from importlib.resources import files
import json

from typing import Any, Dict, List, Optional, Type

from fissg.mimetype import mimetype_pattern
from fissg.knitter import Knitter
from fissg.types.link import LinkProtocol
from fissg.types.metadata import status_types

type langcode = str


@dataclass
class Config(JSONWizard):
    class _(JSONWizard.Meta):
        tag_key = 'type'
        key_transform_with_load = "NONE"
        key_transform_with_dump = "NONE"

    lang: LangConfig
    knitters: Dict[mimetype_pattern, Dict[str, knitter_configs]]
    link_protocols: Dict[str, LinkProtocol]

    pandoc_bin: Optional[str] = None
    default_category: str = "misc"
    tag_slug_prefix: str = "tag-"

    default_page_status: status_types = "published"

    def get_prefixes_for_link_protocol(self, protocol: LinkProtocol) -> List[str]:
        """
        Get all prefixes that are configured a specific link protocol.

        This is used to generate "all" possible links to every linkable object.
        Thus ensuring that all objects have at least one .incoming_link element and that templates in the theme can use links.
        """
        res = sorted(map(lambda x: x[0], filter(lambda x: x[1] == protocol, self.link_protocols.items())))
        if len(res) == 0:
            raise Exception(f"Config hasn't configured any scheme for {protocol} in link_protocols.", self.link_protocols)
        return res


@dataclass
class LangConfig():
    default: langcode
    supported: List[langcode]
    translations: Dict[langcode, Dict]


@dataclass(kw_only=True)
class KnitterConfig(ABC):
    """
    The base class for knitter configs.
    """
    class _(JSONWizard.Meta):
        tag_key = 'type'
    type: str = ""  # This line is just here because dataclass-wizard is dumb when using raise_on_unknown_json_key.

    priority: Optional[int | float] = None
    """
    The priority in which knitters should be executed.

    A higher value means the knitter is executed earlier.
    If two knitters have the same priority their execution order should be asumed to be non-deterministic or even in parallel.

    This is useful as some knitters may depend on other knitters to be executed first.

    If value is None, then the knitter config should set appropriate default values. (=> 0)
    """

    knitter_class: Optional[str] = None
    """
    The path to the class extending fissg.knitter.Knitter. E.g. "fissg.knitters.pandoc.PandocKnitter"

    The path is module_name + "." + class_name.

    If value is None, then the knitter config should set appropriate default values.
    """

    enabled: Optional[bool] = None
    """
    Whether this knitter is enabled.

    If value is None, then the knitter config should set appropriate default values. (=> True)
    """

    def __post_init__(self):
        # Set default values
        if self.priority is None:
            self.priority = 0
        if self.enabled is None:
            self.enabled = True

    def get_knitter_class(self) -> Type[Knitter]:
        assert self.knitter_class is not None
        knitter_class_list = self.knitter_class.split(".")
        module_name = ".".join(knitter_class_list[0:-1])
        class_name = knitter_class_list[-1]
        module = importlib.import_module(module_name, package=None)
        obj = getattr(module, class_name)
        assert issubclass(obj, Knitter), f"'{self.knitter_class}' is not a class extending fissg.knitter.Knitter."
        return obj


@dataclass(kw_only=True)
class MetadataKnitterConfig(KnitterConfig, JSONWizard):
    """
    A configuration for fissg.knitters.metadata.MetadataKnitter.
    """
    class _(JSONWizard.Meta):
        tag = 'metadata'

    def __post_init__(self):
        # Set default values
        if self.priority is None:
            self.priority = 100
        super().__post_init__()
        if self.knitter_class is None:
            self.knitter_class = "fissg.knitters.metadata.MetadataKnitter"


@dataclass(kw_only=True)
class ImageKnitterConfig(KnitterConfig, JSONWizard):
    """
    A configuration for fissg.knitters.image.ImageKnitter.
    """
    class _(JSONWizard.Meta):
        tag = 'image'

    def __post_init__(self):
        # Set default values
        super().__post_init__()
        if self.knitter_class is None:
            self.knitter_class = "fissg.knitters.image.ImageKnitter"


@dataclass(kw_only=True)
class PandocKnitterConfig(KnitterConfig, JSONWizard):
    """
    A configuration for fissg.knitters.pandoc.PandocKnitter.
    """
    class _(JSONWizard.Meta):
        tag = 'pandoc'

    def __post_init__(self):
        # Set default values
        super().__post_init__()
        if self.knitter_class is None:
            self.knitter_class = "fissg.knitters.pandoc.PandocKnitter"

    # Custom attributes
    base: str
    """
    The base format without extensions for the --from argument. E.g. "markdown" or "html5"

    See https://pandoc.org/MANUAL.html#option--from
    """

    extensions: Dict[str, bool | None] = field(default_factory=dict)
    """
    The extensions that should be explicitly enabled (true) or explicitly disabled (false).

    A value of None means the extension will neither be explicitly enabled nor disabled. (It won't be added to the --from argument at all.)

    See https://pandoc.org/MANUAL.html#extensions
    """

    pandoc_bin: Optional[str] = None
    """
    The name or path of the pandoc binary to use.

    This will be given to subprocess.Popen().
    """

    extra_args: List[str] = field(default_factory=list)
    """
    Extra arguments that will be given to the pandoc command.
    """


knitter_configs = MetadataKnitterConfig | ImageKnitterConfig | PandocKnitterConfig
"""
The defined knitter configs.

Note: Just adding a new class that extends KnitterConfig is not enough for JSONWizard to be able to knit it. It must also be specified in the type union variable 'knitters'.

Warning: DO NOT ADD THE type KEYWORD IN FRONT OF knitters. JSONWizard cannot understand the type if you do that.
"""


def read_config(config_file: Path, raise_on_unknown_json_key: bool) -> Config:
    """
    Read the base config from the package and combine it with the config file.
    """
    if not config_file.exists() or not config_file.is_file():
        raise Exception(f"Config file '{config_file}' either doesn't exist or isn't a file.")

    config_dict = {}
    # https://setuptools.pypa.io/en/latest/userguide/datafiles.html#accessing-data-files-at-runtime
    config_dict = combine(config_dict, json.loads(files('fissg.data').joinpath("config.json").read_text()))
    config_dict = combine(config_dict, json.loads(config_file.read_text()))

    LoadMeta(raise_on_unknown_json_key=raise_on_unknown_json_key).bind_to(Config)
    return Config.from_dict(config_dict)


def combine(old: Dict | Any, new: Dict | Any) -> Dict | Any:
    """
    Combine two dictionaries recursivly.

    If old and new are not dictionaries, then just new will be returned unchangend.
    """
    if type(old) is not type(new):
        raise Exception("Cannot combine different types: ", type(old), type(new), old, new)
    if isinstance(old, dict):
        res = old.copy()
        for key, value in new.items():
            if key in res:
                res[key] = combine(res[key], value)
            else:
                res[key] = value
        return res
    else:
        return new
