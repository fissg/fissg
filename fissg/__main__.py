#!/usr/bin/env python3

import argparse
from pathlib import Path, PurePath
import logging
import sys
import time
from typing import Dict, List
import concurrent.futures
# import panflute as pf

from fissg.config import read_config
from fissg.cupboard import Cupboard
from fissg.knitter import Knitter
from fissg.builder import builders
from fissg.config import Config
from fissg.types.file import File
from fissg.theme import Theme


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def read_content_dir(content_dir: Path, config: Config) -> Dict[str, File]:
    """
    Reads all files from the content directory.
    """
    logger.info("Reading content dir: %s", content_dir)
    res: Dict[str, File] = {}
    t1 = time.perf_counter(), time.process_time()

    def create_file(relative: Path, absolute: Path) -> File:
        logger.debug("Reading file: %s", relative)
        return File(relative, absolute, config)

    with concurrent.futures.ThreadPoolExecutor() as executor:
        futures = []
        for path in content_dir.rglob('*'):
            if not path.is_file():
                continue
            absolute = path.resolve()
            relative = path.relative_to(content_dir)
            if any(map(lambda f: f.startswith("."), relative.parts)):
                continue  # ignore hidden files and files in hidden directories
            futures.append(executor.submit(create_file, relative, absolute))

        for future in concurrent.futures.as_completed(futures):
            file = future.result()
            if file.name in res:
                logger.critical("Duplicate file found! '%s' and '%s' have the same name which is not allowed.", res[file.name].path_in_content_dir, file.path_in_content_dir)
                sys.exit(1)
            res[file.name] = file

    t2 = time.perf_counter(), time.process_time()
    logger.info("Reading %d files from content dir took %.2f seconds (cpu: %.2f seconds)", len(res), t2[0] - t1[0], t2[1] - t1[1])
    return res


def execute_knitters(cupboard: Cupboard):
    t1_total = time.perf_counter(), time.process_time()

    # Initialize Knitter instances
    knitters: List[Knitter] = []
    for mimetypes, inner_dict in cupboard.config.knitters.items():
        for name, knitter_config in inner_dict.items():
            if not knitter_config.enabled:
                continue  # don't add disabled knitters
            knitter_class = knitter_config.get_knitter_class()
            knitter = knitter_class(name, mimetypes, knitter_config, cupboard)
            knitters.append(knitter)

    # sort by configured priority
    knitters.sort(key=lambda knitter: knitter.knitter_config.priority or 0, reverse=True)

    # Execute knitters
    for knitter in knitters:
        logger.info("=" * 40)

        files = list(filter(lambda f: knitter.can_knit(f), cupboard.files.values()))
        logger.info("Using Knitter '%s' (%s) on %d '%s' files.", knitter.name, knitter.knitter_config.knitter_class, len(files), knitter.mimetypes)

        # Iterate over all files and execute the current knitter
        t1 = time.perf_counter(), time.process_time()
        for file in files:
            logger.info("[%s] Knitting '%s' (%s).", knitter.name, file.name, file.mimetype.mimetype)
            knitter.knit(file)  # Execute knitter
        t2 = time.perf_counter(), time.process_time()

        logger.info("Knitting %d files using '%s' took %.2f seconds (cpu: %.2f seconds)", len(files), knitter.name, t2[0] - t1[0], t2[1] - t1[1])
    t2_total = time.perf_counter(), time.process_time()
    logger.info("All knitting in total took %.2f seconds (cpu: %.2f seconds)", t2_total[0] - t1_total[0], t2_total[1] - t1_total[1])


def execute_builders(cupboard: Cupboard, theme: Theme) -> Dict[PurePath, bytes]:
    res: Dict[PurePath, bytes] = {}
    t1 = time.perf_counter(), time.process_time()
    for builder in builders:
        # TODO optimize
        builder(cupboard, theme, res)

    # Validate paths will not escape the output directory
    for path in res.keys():
        if path.is_absolute():
            raise Exception("Builder produced an absolute Path.", path)
        if ".." in path.parts:
            raise Exception("Builder produced a relative path that tries to escape the current directory.", path)

    t2 = time.perf_counter(), time.process_time()
    logger.info("Building %d files took %.2f seconds (cpu: %.2f seconds)", len(res), t2[0] - t1[0], t2[1] - t1[1])
    return res


def write_files(files: Dict[PurePath, bytes], output_dir: Path):
    t1 = time.perf_counter(), time.process_time()
    output_dir.mkdir(parents=True, exist_ok=True)
    written = 0
    for path, content in files.items():
        path = output_dir / path
        path.parent.mkdir(parents=True, exist_ok=True)
        if path.exists():
            read_bytes = path.read_bytes()
            if len(read_bytes) == len(content) and read_bytes == content:
                continue
        path.write_bytes(content)
        written += 1
    t2 = time.perf_counter(), time.process_time()
    logger.info("Writing %d of %d files took %.2f seconds (cpu: %.2f seconds)", written, len(files), t2[0] - t1[0], t2[1] - t1[1])


def main():
    """
    This function is defined to be the main entry point for the `fissg`
    command in setup.py.
    """
    parser = argparse.ArgumentParser(
        prog='fissg',
        description='FISSG Is a Static-Site-Generator',
    )
    parser.add_argument('--test', action='store_true', help="Test the command.")
    parser.add_argument('-c', '--content-dir', type=Path, help="Content directory", default="./content")
    parser.add_argument('-o', '--output-dir', type=Path, help="Output directory", default="./public")
    parser.add_argument('-t', '--theme-dir', type=Path, help="Theme directory", default="./theme")
    parser.add_argument('--config-file', type=Path, help="JSON Config file.", default="./content/config.json")
    parser.add_argument('--disable-exception-on-unknown-config-key', action='store_true', help="Disables checking for unknown keys in config. (Disables the 'dataclass_wizard.errors.UnknownJSONKey: A JSON key is missing from the dataclass schema for class ...' error.)")

    args = parser.parse_args()

    if args.test:
        logger.debug("Testing...")

    logger.info("Hello World")

    content_dir: Path = args.content_dir
    if not content_dir.exists() or not content_dir.is_dir():
        logger.critical("content dir \"%s\" does not exist or is not a directory.", content_dir)
        sys.exit(1)

    output_dir: Path = args.output_dir
    if not output_dir.exists() and not content_dir.is_dir():
        logger.critical("output dir \"%s\" does exist and is not a directory.", output_dir)
        sys.exit(1)

    theme_dir: Path = args.theme_dir
    if not theme_dir.exists() or not theme_dir.is_dir():
        logger.critical("theme dir \"%s\" does not exist or is not a directory.", theme_dir)
        sys.exit(1)

    config_file: Path = args.config_file
    config = read_config(config_file, not args.disable_exception_on_unknown_config_key)
    print(config.to_json(indent=4))

    theme = Theme(theme_dir, config)

    content_files = read_content_dir(content_dir, config)
    theme_files = theme.read_content_files()

    for f in theme_files.keys():
        if f in content_files:
            logger.warning("Content file included in the theme is being overriden by file with same name in content dir: %s", f)
    files = {**theme_files, **content_files}

    cupboard = Cupboard(config=config, files=files)
    logger.debug("Mimetypes found: %s", sorted(set(map(lambda f: f.mimetype.mimetype or "UNKNOWN", cupboard.files.values()))))

    execute_knitters(cupboard)

    # links = set()
    # def action(elem: pf.Element, doc: pf.Doc) -> None | pf.Element:
    #     if not (isinstance(elem, pf.Link) or isinstance(elem, pf.Image)):
    #         return
    #     print(elem.url, elem.title, type(elem))
    #     links.add(elem.url)
    # for page in cupboard.pages.values():
    #     print("=" * 80)
    #     print(page.slug)
    #     for lang, document in page.translations.items():
    #         print("=" * 40)
    #         print(page.slug, lang)
    #         document.content.walk(action)
    # print("=" * 80)
    # for link in sorted(links):
    #     print(link)
    for tag in cupboard.tags.values():
        print("=" * 80)
        print(tag.slug)
        for page in tag.pages:
            print("  ", page.slug)

    finished_files = execute_builders(cupboard, theme)

    write_files(finished_files, output_dir)


if __name__ == "__main__":
    """
    This part is not the actual main. This is just useful for testing.
    """
    main()
